<?php

namespace Altum\Controllers;

use Altum\Database\Database;
use Altum\Middlewares\Authentication;
use Altum\Middlewares\Csrf;
use Altum\Models\User;
use Altum\Title;

class Campaign extends Controller {

    public function index() {

        Authentication::guard();

        $campaign_id = isset($this->params[0]) ? (int) $this->params[0] : false;

        /* Make sure the campaign exists and is accessible to the user */
        if(!$campaign = Database::get('*', 'campaigns', ['campaign_id' => $campaign_id, 'user_id' => $this->user->user_id])) {
            redirect('dashboard');
        }

        /* Get the custom branding details */
        $campaign->branding = json_decode($campaign->branding);

        /* Get the campaigns list for the user */
        $notifications_result = Database::$database->query("SELECT * FROM `notifications` WHERE `campaign_id` = {$campaign->campaign_id} AND `user_id` = {$this->user->user_id}");

        /* Make sure that the user didn't exceed the limit */
        $notifications_total = $notifications_result->num_rows;

        /* Custom Branding Campaign Modal */
        if($this->user->package_settings->custom_branding) {
            $data = ['campaign' => $campaign];
            $view = new \Altum\Views\View('campaign/custom_branding_campaign_modal', (array) $this);
            \Altum\Event::add_content($view->run($data), 'modals');
        }

        /* Pixel Modal */
        $view = new \Altum\Views\View('campaign/campaign_pixel_key_modal', (array) $this);
        \Altum\Event::add_content($view->run(), 'modals');

        /* Update Campaign Modal */
        $view = new \Altum\Views\View('campaign/update_campaign_modal', (array) $this);
        \Altum\Event::add_content($view->run(), 'modals');

        /* Prepare the View */
        $data = [
            'campaign'               => $campaign,
            'notifications_result'   => $notifications_result,
            'notifications_total'    => $notifications_total
        ];

        $view = new \Altum\Views\View('campaign/index', (array) $this);

        $this->add_view_content('content', $view->run($data));

        /* Set a custom title */
        Title::set(sprintf($this->language->campaign->title, $campaign->name));

    }

}
