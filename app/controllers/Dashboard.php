<?php

namespace Altum\Controllers;

use Altum\Database\Database;
use Altum\Middlewares\Authentication;
use Altum\Models\Package;
use Altum\Routing\Router;

class Dashboard extends Controller {

    public function index() {

        Authentication::guard();

        /* Create Campaign Modal */
        $view = new \Altum\Views\View('campaign/create_campaign_modal', (array) $this);
        \Altum\Event::add_content($view->run(), 'modals');

        /* Update Campaign Modal */
        $view = new \Altum\Views\View('campaign/update_campaign_modal', (array) $this);
        \Altum\Event::add_content($view->run(), 'modals');

        /* Custom Branding Campaign Modal */
        if($this->user->package_settings->custom_branding) {
            $view = new \Altum\Views\View('campaign/custom_branding_campaign_modal', (array)$this);
            \Altum\Event::add_content($view->run(), 'modals');
        }

        /* Pixel Modal */
        $view = new \Altum\Views\View('campaign/campaign_pixel_key_modal', (array) $this);
        \Altum\Event::add_content($view->run(), 'modals');

        /* Get the campaigns list for the user */
        $campaigns_result = Database::$database->query("SELECT * FROM `campaigns` WHERE `user_id` = {$this->user->user_id}");

        /* Prepare the View */
        $data = [
            'campaigns_result' => $campaigns_result,
            'campaigns_total' => $campaigns_result->num_rows
        ];

        $view = new \Altum\Views\View('dashboard/index', (array) $this);

        $this->add_view_content('content', $view->run($data));

    }

}
