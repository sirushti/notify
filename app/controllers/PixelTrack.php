<?php

namespace Altum\Controllers;

use Altum\Database\Database;
use Altum\Language;
use Unirest\Request;

class PixelTrack extends Controller {

    public function index() {

        if(!isset($_SERVER['HTTP_REFERER'])) {
            die();
        }

        /* Allowed types of requests to this endpoint */
        $allowed_types = ['track', 'notification', 'auto_capture', 'collector'];
        $date = \Altum\Date::$date;
        $domain = Database::clean_string(parse_url(trim(Database::clean_string($_SERVER['HTTP_REFERER'])))['host']);
        $pixel_key = Database::clean_string($_GET['pixel_key']);

        if(!isset($_GET['type']) || isset($_GET['type']) && !in_array($_GET['type'], $allowed_types)) {
            die();
        }

        /* Flatten everything recursively */
        $_GET = array_flatten($_GET);

        /* Clean all the received variables */
        foreach($_GET as $key => $value) {

            /* Whitelist */
            if(in_array($key, ['location'])) {
                continue;
            }

            $_GET[$key] = Database::clean_string($value);
        }

        $_GET['current_page'] = urldecode($_GET['current_page']);

        /* Make sure the user and the campaign exist and are good to go */
        $campaign = $this->database->query("
            SELECT
                `users`.`user_id`,
                `users`.`package_settings`,
                `users`.`language`,
                `users`.`current_month_notifications_impressions`,                
                `campaigns`.`campaign_id`,
                `campaigns`.`branding`
            FROM
                `users`
            LEFT JOIN
                `campaigns` ON `users`.`user_id` = `campaigns`.`user_id`
            WHERE
                `users`.`active` = 1 AND
                `campaigns`.`pixel_key` = '{$pixel_key}' AND  
                `campaigns`.`is_enabled` = 1 AND 
                (
                    (`campaigns`.`include_subdomains` = 0 AND `campaigns`.`domain` = '{$domain}' OR `campaigns`.`domain` = 'www.{$domain}')
                    OR
                    (`campaigns`.`include_subdomains` = 1 AND '{$domain}' LIKE CONCAT('%', `campaigns`.`domain`))
                )
        ")->fetch_object() ?? null;

        if(!$campaign) {
            die();
        }

        switch($_GET['type']) {

            /* Tracking the notifications states, impressions, hovers..etc */
            case 'notification':

                /* Generate an id for the log */
                $dynamic_id = md5(
                    $_GET['notification_id'] . $_GET['subtype'] . $_GET['ip'] . (new \DateTime())->format('Y-m-d') . $_GET['agent'] . $_GET['current_page']
                );

                $_GET['notification_id'] = (int) $_GET['notification_id'];
                $_GET['subtype'] = in_array(
                    $_GET['subtype'],
                    [
                        'hover',
                        'impression',
                        'click',
                        'feedback_emoji_angry',
                        'feedback_emoji_sad',
                        'feedback_emoji_neutral',
                        'feedback_emoji_happy',
                        'feedback_emoji_excited',
                        'feedback_score_1',
                        'feedback_score_2',
                        'feedback_score_3',
                        'feedback_score_4',
                        'feedback_score_5'
                    ]
                ) ? $_GET['subtype'] : false;

                /* Make sure the type of notification is the correct one */
                if(!$_GET['subtype']) {
                    die();
                }

                /* Make sure the notification provided is a child of the campaign, exists and is enabled */
                if(!$notification = Database::exists('notification_id', 'notifications', ['notification_id' => $_GET['notification_id'], 'campaign_id' => $campaign->campaign_id, 'is_enabled' => 1])) {
                    die();
                }

                /* Insert or update the log */
                $stmt = Database::$database->prepare("
                    INSERT INTO 
                        `track_notifications` (`notification_id`, `dynamic_id`, `type`, `ip`, `url`, `date`, `last_date`) 
                    VALUES 
                        (?, ?,  ?, ?, ?, ?, ?)
                    ON DUPLICATE KEY UPDATE
                        `count` = `count` + 1,
                        `last_date` = VALUES (last_date)  
                ");
                $stmt->bind_param(
                    'sssssss',
                    $_GET['notification_id'],
                    $dynamic_id,
                    $_GET['subtype'],
                    $_GET['ip'],
                    $_GET['current_page'],
                    $date,
                    $date
                );
                $stmt->execute();
                $stmt->close();

                /* Count it in the users account if its an impression */
                if($_GET['subtype'] == 'impression') {
                    $stmt = Database::$database->prepare("UPDATE `users` SET `current_month_notifications_impressions` = `current_month_notifications_impressions` + 1, `total_notifications_impressions` = `total_notifications_impressions` + 1 WHERE `user_id` = ?");
                    $stmt->bind_param('s', $campaign->user_id);
                    $stmt->execute();
                    $stmt->close();
                }

                break;

            /* Tracking the visits of the user */
            case 'track':

                /* Generate an id for the log */
                $dynamic_id = md5(
                    $_GET['ip'] . (new \DateTime())->format('Y-m-d') . $_GET['agent'] . $_GET['current_page']
                );
                $details = json_encode($_GET);

                /* Insert or update the log */
                $stmt = Database::$database->prepare("
                    INSERT INTO 
                        `track_logs` (`user_id`, `domain`, `dynamic_id`, `ip`, `url`, `details`, `date`, `last_date`) 
                    VALUES 
                        (?, ?, ?, ?, ?, ?, ?, ?)
                    ON DUPLICATE KEY UPDATE
                        `details` = VALUES (details),
                        `count` = `count` + 1,
                        `last_date` = VALUES (last_date)    
                ");
                $stmt->bind_param(
                    'ssssssss',
                    $campaign->user_id,
                    $domain,
                    $dynamic_id,
                    $_GET['ip'],
                    $_GET['current_page'],
                    $details,
                    $date,
                    $date
                );
                $stmt->execute();
                $stmt->close();

                break;

            /* Getting the data from the email collector form */
            case 'collector':

                $_GET['notification_id'] = (int) $_GET['notification_id'];

                /* Determine if we have email or input keys */
                $collector_key = false;

                if(isset($_GET['email']) && !empty($_GET['email'])) {
                    $collector_key = 'email';

                    /* Make sure that what we got is an actual email */
                    if(!filter_var($_GET['email'], FILTER_VALIDATE_EMAIL)) {
                        die();
                    }
                }

                if(isset($_GET['input']) && !empty($_GET['input'])) {
                    $collector_key = 'input';
                }

                if(!$collector_key) {
                    die();
                }

                /* Make sure that the data is not already submitted and exists for this notification */
                $result = Database::$database->query("SELECT `id` FROM `track_conversions` WHERE `notification_id` = {$_GET['notification_id']} AND JSON_EXTRACT(`data`, '$.{$collector_key}') = '{$_GET[$collector_key]}'");

                if($result->num_rows) {
                    die();
                }

                /* Try and get the location data sent */
                $location_data = null;

                if($_GET['location']) {

                    try {
                        $location_data = json_decode($_GET['location']);

                        $location_data = json_encode(
                            Database::clean_array([
                                'city' => $location_data->city,
                                'country_code' => $location_data->country_code,
                                'country' => $location_data->country
                            ])
                        );

                    } catch (\Exception $exception) {
                        $location_data = null;
                    }

                }

                /* Data for the conversion */
                $data = json_encode([
                    $collector_key => $_GET[$collector_key]
                ]);

                /* Insert the conversion log */
                $stmt = Database::$database->prepare("
                    INSERT INTO
                        `track_conversions` (`notification_id`, `type`, `data`, `ip`, `url`, `location`, `date`)
                    VALUES
                        (?, ?, ?, ?, ?, ?, ?)
                ");
                $stmt->bind_param(
                    'sssssss',
                    $_GET['notification_id'],
                    $_GET['type'],
                    $data,
                    $_GET['ip'],
                    $_GET['current_page'],
                    $location_data,
                    $date
                );
                $stmt->execute();
                $stmt->close();

                /* Insert the log in the notification tracking table */
                /* Generate an id for the log */
                $type = 'form_submission';
                $dynamic_id = md5(
                    $_GET['notification_id'] . $type . $_GET['ip'] . (new \DateTime())->format('Y-m-d') . $_GET['agent'] . $_GET['current_page']
                );

                /* Insert or update the log */
                $stmt = Database::$database->prepare("
                    INSERT INTO
                        `track_notifications` (`notification_id`, `dynamic_id`, `type`, `ip`, `url`, `date`, `last_date`)
                    VALUES
                        (?, ?,  ?, ?, ?, ?, ?)
                    ON DUPLICATE KEY UPDATE
                        `count` = `count` + 1,
                        `last_date` = VALUES (last_date)
                ");
                $stmt->bind_param(
                    'sssssss',
                    $_GET['notification_id'],
                    $dynamic_id,
                    $type,
                    $_GET['ip'],
                    $_GET['current_page'],
                    $date,
                    $date
                );
                $stmt->execute();
                $stmt->close();

                /* Make sure to send the webhook of the conversion */
                $notification = Database::$database->query("SELECT `notifications`.`name`, `notifications`.`settings`, `campaigns`.`name` AS `campaign_name` FROM `notifications` LEFT JOIN `campaigns` ON `campaigns`.`campaign_id` = `notifications`.`campaign_id`  WHERE `notification_id` = {$_GET['notification_id']}")->fetch_object();
                $notification->settings = json_decode($notification->settings);

                /* Only send if we need to */
                if($notification->settings->data_send_is_enabled) {

                    /* Webhook POST to the url the user specified */
                    if(!empty($notification->settings->data_send_webhook)) {

                        /* Send the webhook with the caught details */
                        $body = Request\Body::form([$collector_key => $_GET[$collector_key]]);

                        $response = Request::post($notification->settings->data_send_webhook, [], $body);
                    }

                    /* Send email to the url the user specified */
                    if(!empty($notification->settings->data_send_email)) {

                        /* Get the language for the user */
                        $language = Language::get($campaign->language);

                        /* Prepare the html for the email body */
                        $email_body = '<ul>';
                        foreach(array_merge(json_decode($location_data, true), json_decode($data, true), ['ip' => $_GET['ip'], 'url' => $_GET['current_page']]) as $key => $value) {
                            $email_body .= '<li><strong>' . $key . ':</strong>' . ' ' . $value;
                        }
                        $email_body .= '</ul>';

                        send_mail(
                            $this->settings,
                            $notification->settings->data_send_email,
                            sprintf($language->global->emails->user_data_send->subject, $notification->campaign_name, $notification->name),
                            sprintf($language->global->emails->user_data_send->body, $notification->campaign_name, $notification->name, $email_body)
                        );

                    }

                }

                break;

            /* Auto Capturing data from forms */
            case 'auto_capture':

                $_GET['notification_id'] = (int) $_GET['notification_id'];

                /* Make sure to get only the needed data from the submission */
                $data = [];

                /* Save only parameters that start with "form_" */
                foreach($_GET as $key => $value) {
                    if(strpos($key, 'form_') === 0) {
                        $data[str_replace('form_', '', $key)] = $value;
                    }
                }

                /* Data for the conversion */
                $data = json_encode($data);

                /* Try and get the location data sent */
                $location_data = null;

                if($_GET['location']) {

                    try {
                        $location_data = json_decode($_GET['location']);

                        $location_data = json_encode(
                            Database::clean_array([
                                'city' => $location_data->city,
                                'country_code' => $location_data->country_code,
                                'country' => $location_data->country
                            ])
                        );

                    } catch (\Exception $exception) {
                        $location_data = null;
                    }

                }

                /* Insert the conversion log */
                $stmt = Database::$database->prepare("
                    INSERT INTO 
                        `track_conversions` (`notification_id`, `type`, `data`, `ip`, `url`, `location`, `date`) 
                    VALUES 
                        (?, ?, ?, ?, ?, ?, ?)
                ");
                $stmt->bind_param(
                    'sssssss',
                    $_GET['notification_id'],
                    $_GET['type'],
                    $data,
                    $_GET['ip'],
                    $_GET['current_page'],
                    $location_data,
                    $date
                );
                $stmt->execute();
                $stmt->close();

                /* Insert the log in the notification tracking table */
                /* Generate an id for the log */
                $type = 'auto_capture';
                $dynamic_id = md5(
                    $_GET['notification_id'] . $type . $_GET['ip'] . (new \DateTime())->format('Y-m-d') . $_GET['agent'] . $_GET['current_page']
                );

                /* Insert or update the log */
                $stmt = Database::$database->prepare("
                    INSERT INTO 
                        `track_notifications` (`notification_id`, `dynamic_id`, `type`, `ip`, `url`, `date`, `last_date`) 
                    VALUES 
                        (?, ?,  ?, ?, ?, ?, ?)
                    ON DUPLICATE KEY UPDATE
                        `count` = `count` + 1,
                        `last_date` = VALUES (last_date)  
                ");
                $stmt->bind_param(
                    'sssssss',
                    $_GET['notification_id'],
                    $dynamic_id,
                    $type,
                    $_GET['ip'],
                    $_GET['current_page'],
                    $date,
                    $date
                );
                $stmt->execute();
                $stmt->close();

                /* Update the notification with the last conversion date */
                $stmt = Database::$database->prepare("UPDATE `notifications` SET `last_action_date` = ? WHERE `notification_id` = ? ");
                $stmt->bind_param(
                    'ss',
                    $date,
                    $_GET['notification_id']
                );
                $stmt->execute();
                $stmt->close();

                break;
        }

        die();
    }

}
