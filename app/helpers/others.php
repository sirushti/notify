<?php

/* Generate chart data for based on the date key and each of keys inside */
function get_chart_data(Array $main_array) {

    $results = [];

    foreach($main_array as $date_label => $data) {

        foreach($data as $label_key => $label_value) {

            if(!isset($results[$label_key])) {
                $results[$label_key] = [];
            }

            $results[$label_key][] = $label_value;

        }

    }

    foreach($results as $key => $value) {
        $results[$key] = '["' . implode('", "', $value) . '"]';
    }

    $results['labels'] = '["' . implode('", "', array_keys($main_array)) . '"]';

    return $results;
}

function get_gravatar($email, $s = 80, $d = 'mp', $r = 'g', $img = false, $atts = []) {
    $url = 'https://www.gravatar.com/avatar/';
    $url .= md5(strtolower(trim($email)));
    $url .= "?s=$s&d=$d&r=$r";

    if ($img) {
        $url = '<img src="' . $url . '"';

        foreach ($atts as $key => $val) {
            $url .= ' ' . $key . '="' . $val . '"';
        }

        $url .= ' />';
    }

    return $url;
}

function get_admin_options_button($type, $target_id) {

    switch($type) {

        case 'user' :
            return '
                <div class="dropdown">
                    <a href="#" data-toggle="dropdown" class="text-secondary dropdown-toggle dropdown-toggle-simple">
                        <i class="fa fa-fw fa-ellipsis-v"></i>
                        
                        <div class="dropdown-menu dropdown-menu-right">
                            <a class="dropdown-item" href="admin/user-view/' . $target_id . '"><i class="fa fa-fw fa-sm fa-eye mr-1"></i> ' . \Altum\Language::get()->global->view . '</a>
                            <a class="dropdown-item" href="admin/user-update/' . $target_id . '"><i class="fa fa-fw fa-sm fa-pencil-alt mr-1"></i> ' . \Altum\Language::get()->global->edit . '</a>
                            <a class="dropdown-item" data-confirm="' . \Altum\Language::get()->global->info_message->confirm_delete . '" href="admin/users/delete/' . $target_id . \Altum\Middlewares\Csrf::get_url_query() . '"><i class="fa fa-fw fa-sm fa-times mr-1"></i> ' . \Altum\Language::get()->global->delete . '</a>
                            <a href="#" data-toggle="modal" data-target="#user_login" data-user-id="' . $target_id . '" class="dropdown-item"><i class="fa fa-fw fa-sm fa-sign-in-alt mr-1"></i> ' . \Altum\Language::get()->global->login . '</a>
                        </div>
                    </a>
                </div>';

            break;


        case 'pages_category' :
            return '
                <div class="dropdown">
                    <a href="#" data-toggle="dropdown" class="text-secondary dropdown-toggle dropdown-toggle-simple">
                        <i class="fa fa-fw fa-ellipsis-v"></i>
                        
                        <div class="dropdown-menu dropdown-menu-right">
                            <a class="dropdown-item" href="admin/pages-category-update/' . $target_id . '"><i class="fa fa-fw fa-sm fa-pencil-alt mr-1"></i> ' . \Altum\Language::get()->global->edit . '</a>
                            <a href="#" data-toggle="modal" data-target="#pages_category_delete" data-pages-category-id="' . $target_id . '" class="dropdown-item"><i class="fa fa-fw fa-sm fa-times mr-1"></i> ' . \Altum\Language::get()->global->delete . '</a>
                        </div>
                    </a>
                </div>';

            break;

        case 'page' :
            return '
                <div class="dropdown">
                    <a href="#" data-toggle="dropdown" class="text-secondary dropdown-toggle dropdown-toggle-simple">
                        <i class="fa fa-fw fa-ellipsis-v"></i>
                        
                        <div class="dropdown-menu dropdown-menu-right">
                            <a class="dropdown-item" href="admin/page-update/' . $target_id . '"><i class="fa fa-fw fa-sm fa-pencil-alt mr-1"></i> ' . \Altum\Language::get()->global->edit . '</a>
                            <a href="#" data-toggle="modal" data-target="#page_delete" data-page-id="' . $target_id . '" class="dropdown-item"><i class="fa fa-fw fa-sm fa-times mr-1"></i> ' . \Altum\Language::get()->global->delete . '</a>
                        </div>
                    </a>
                </div>';

            break;

        case 'package' :
            return '
                <div class="dropdown">
                    <a href="#" data-toggle="dropdown" class="text-secondary dropdown-toggle dropdown-toggle-simple">
                        <i class="fa fa-fw fa-ellipsis-v"></i>
                        
                        <div class="dropdown-menu dropdown-menu-right">
                            <a class="dropdown-item" href="admin/package-update/' . $target_id . '"><i class="fa fa-fw fa-sm fa-pencil-alt mr-1"></i> ' . \Altum\Language::get()->global->edit . '</a>
                            
                            ' . (is_numeric($target_id) ?
                                '<a class="dropdown-item" data-confirm="' . \Altum\Language::get()->global->info_message->confirm_delete . '" href="admin/packages/delete/' . $target_id . \Altum\Middlewares\Csrf::get_url_query() . '"><i class="fa fa-fw fa-sm fa-times mr-1"></i> ' . \Altum\Language::get()->global->delete . '</a>'
                                : null) . '
                            
                        </div>
                    </a>
                </div>';

            break;

        case 'code' :
            return '
                <div class="dropdown">
                    <a href="#" data-toggle="dropdown" class="text-secondary dropdown-toggle dropdown-toggle-simple">
                        <i class="fa fa-fw fa-ellipsis-v"></i>
                        
                        <div class="dropdown-menu dropdown-menu-right">
                            <a class="dropdown-item" href="admin/code-update/' . $target_id . '"><i class="fa fa-fw fa-sm fa-pencil-alt mr-1"></i> ' . \Altum\Language::get()->global->edit . '</a>
                            <a href="#" data-toggle="modal" data-target="#code_delete" data-code-id="' . $target_id . '" class="dropdown-item"><i class="fa fa-fw fa-sm fa-times mr-1"></i> ' . \Altum\Language::get()->global->delete . '</a>
                        </div>
                    </a>
                </div>';

            break;

    }
}

/* Helper to output proper and nice numbers */
function nr($number, $decimals = 0, $extra = false) {

    if($extra) {
        $formatted_number = $number;
        $touched = false;

        if(!$touched && (!is_array($extra) || (is_array($extra) && in_array('B', $extra)))) {

            if($number > 999999999) {
                $formatted_number = number_format($number / 1000000000, $decimals, \Altum\Language::get()->global->number->decimal_point, \Altum\Language::get()->global->number->thousands_separator) . 'B';

                $touched = true;
            }

        }

        if(!$touched && (!is_array($extra) || (is_array($extra) && in_array('M', $extra)))) {

            if($number > 999999) {
                $formatted_number = number_format($number / 1000000, $decimals, \Altum\Language::get()->global->number->decimal_point, \Altum\Language::get()->global->number->thousands_separator) . 'M';

                $touched = true;
            }

        }

        if(!$touched && (!is_array($extra) || (is_array($extra) && in_array('K', $extra)))) {

            if($number > 999) {
                $formatted_number = number_format($number / 1000, $decimals, \Altum\Language::get()->global->number->decimal_point, \Altum\Language::get()->global->number->thousands_separator) . 'K';

                $touched = true;
            }

        }

        if($decimals > 0) {
            $dotzero = '.' . str_repeat('0', $decimals);
            $formatted_number = str_replace($dotzero, '', $formatted_number);
        }

        return $formatted_number;
    }

    if($number == 0) {
        return 0;
    }

    return number_format($number, $decimals, \Altum\Language::get()->global->number->decimal_point, \Altum\Language::get()->global->number->thousands_separator);
}

function get_ip() {
    if(array_key_exists('HTTP_X_FORWARDED_FOR', $_SERVER)) {

        if(strpos($_SERVER['HTTP_X_FORWARDED_FOR'], ',')) {
            $ips = explode(',', $_SERVER['HTTP_X_FORWARDED_FOR']);

            return trim(reset($ips));
        } else {
            return $_SERVER['HTTP_X_FORWARDED_FOR'];
        }

    } else if (array_key_exists('REMOTE_ADDR', $_SERVER)) {
        return $_SERVER['REMOTE_ADDR'];
    } else if (array_key_exists('HTTP_CLIENT_IP', $_SERVER)) {
        return $_SERVER['HTTP_CLIENT_IP'];
    }

    return '';
}

function csv_exporter($array, $exclude = []) {

    $result = '';

    /* Get the total amount of columns */
    $columns_count = count((array) reset($array));

    /* Export the header */
    $i = 0;
    foreach(array_keys((array) reset($array)) as $value) {
        /* Check if not excluded */
        if(!in_array($value, $exclude)) {
            $result .= $i++ !== $columns_count - 1 ? $value . ',' : $value;
        }
    }

    foreach($array as $row) {
        $result .= "\n";

        $i = 0;
        foreach($row as $key => $value) {
            /* Check if not excluded */
            if(!in_array($key, $exclude)) {

                $value = addslashes($value);

                $result .= $i++ !== $columns_count - 1 ? '"' . $value . '"' . ',' : '"' . $value . '"';
            }
        }
    }

    return $result;
}

function csv_link_exporter($csv) {
    return 'data:application/csv;charset=utf-8,' . urlencode($csv);
}

function array_flatten($array, $prefix = '') {
    $result = [];

    foreach($array as $key=>$value) {
        if(is_array($value)) {
            $result = $result + array_flatten($value, $prefix . $key . '.');
        }
        else {
            $result[$prefix.$key] = $value;
        }
    }

    return $result;
}

/* Dump & die */
function dd($string = null) {
    var_dump($string);
    die();
}

/* Output in debug.log file */
function dl($string = null) {
    ob_start();

    print_r($string);

    $content = ob_get_clean();

    error_log($content);
}
