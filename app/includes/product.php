<?php
define('PRODUCT_NAME', 'SocialProofo');
define('PRODUCT_KEY', 'socialproof');
define('PRODUCT_URL', 'https://altumco.de/socialproofo-buy');
define('PRODUCT_DOCUMENTATION_URL', 'https://altumco.de/socialproofo-docs');
define('PRODUCT_VERSION', '1.7.7');
define('PRODUCT_CODE', '177');
