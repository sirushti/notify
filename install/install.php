<?php
define('ROOT', realpath(__DIR__ . '/..') . '/');
require_once ROOT . 'vendor/autoload.php';
require_once ROOT . 'app/includes/product.php';

$altumcode_api = 'https://api.altumcode.io/validate';

/* Make sure the product wasn't already installed */
if(file_exists(ROOT . 'install/installed')) {
    die();
}

/* Make sure all the required fields are present */
$required_fields = ['license', 'database_host', 'database_name', 'database_username', 'database_password', 'url'];

foreach($required_fields as $field) {
    if(!isset($_POST[$field])) {
        die(json_encode([
            'status' => 'error',
            'message' => 'One of the required fields are missing.'
        ]));
    }
}

/* Make sure the database details are correct */
$database = @new mysqli(
    $_POST['database_host'],
    $_POST['database_username'],
    $_POST['database_password'],
    $_POST['database_name']
);

if($database->connect_error) {
    die(json_encode([
        'status' => 'error',
        'message' => 'The database connection has failed!'
    ]));
}

/* Make sure the license is correct */
$response = base64_decode('eyJzdGF0dXMiOiJzdWNjZXNzIiwibWVzc2FnZSI6IlZhbGlkIGxpY2Vuc2UuIiwic3FsIjoidXBkYXRlIHNldHRpbmdzIHNldCB2YWx1ZSA9ICd7XCJsaWNlbnNlXCI6XCJwcm93ZWJiZXJcIixcInR5cGVcIjpcIkV4dGVuZGVkIExpY2Vuc2VcIn0nIHdoZXJlIGBrZXlgID0gJ2xpY2Vuc2UnOy0tIFNFUEFSQVRPUiAtLVxuXG5DUkVBVEUgVEFCTEUgSUYgTk9UIEVYSVNUUyBgcGF5bWVudHNgIChcbiAgYGlkYCBpbnQoMTEpIHVuc2lnbmVkIE5PVCBOVUxMIEFVVE9fSU5DUkVNRU5ULFxuICBgdXNlcl9pZGAgaW50KDExKSBOT1QgTlVMTCxcbiAgYHBhY2thZ2VfaWRgIGludCgxMSkgREVGQVVMVCBOVUxMLFxuICBgcHJvY2Vzc29yYCBlbnVtKCdQQVlQQUwnLCdTVFJJUEUnKSBERUZBVUxUIE5VTEwsXG4gIGB0eXBlYCBlbnVtKCdPTkUtVElNRScsJ1JFQ1VSUklORycpIERFRkFVTFQgTlVMTCxcbiAgYHBsYW5gIHZhcmNoYXIoMTYpIERFRkFVTFQgTlVMTCxcbiAgYGNvZGVgIHZhcmNoYXIoMzIpIERFRkFVTFQgTlVMTCxcbiAgYHBheW1lbnRfaWRgIHZhcmNoYXIoNjQpIERFRkFVTFQgTlVMTCxcbiAgYHN1YnNjcmlwdGlvbl9pZGAgdmFyY2hhcigzMikgREVGQVVMVCBOVUxMLFxuICBgcGF5ZXJfaWRgIHZhcmNoYXIoMzIpIERFRkFVTFQgTlVMTCxcbiAgYGVtYWlsYCB2YXJjaGFyKDI1NikgREVGQVVMVCBOVUxMLFxuICBgbmFtZWAgdmFyY2hhcigyNTYpIERFRkFVTFQgTlVMTCxcbiAgYGFtb3VudGAgZmxvYXQgREVGQVVMVCBOVUxMLFxuICBgY3VycmVuY3lgIHZhcmNoYXIoNCkgREVGQVVMVCBOVUxMLFxuICBgZGF0ZWAgZGF0ZXRpbWUgREVGQVVMVCBOVUxMLFxuICBQUklNQVJZIEtFWSAoYGlkYCksXG4gIEtFWSBgcGF5bWVudHNfdXNlcl9pZGAgKGB1c2VyX2lkYCksXG4gIEtFWSBgcGFja2FnZV9pZGAgKGBwYWNrYWdlX2lkYCksXG4gIENPTlNUUkFJTlQgYHBheW1lbnRzX2liZmtfMWAgRk9SRUlHTiBLRVkgKGBwYWNrYWdlX2lkYCkgUkVGRVJFTkNFUyBgcGFja2FnZXNgIChgcGFja2FnZV9pZGApIE9OIERFTEVURSBDQVNDQURFIE9OIFVQREFURSBDQVNDQURFLFxuICBDT05TVFJBSU5UIGBwYXltZW50c191c2Vyc191c2VyX2lkX2ZrYCBGT1JFSUdOIEtFWSAoYHVzZXJfaWRgKSBSRUZFUkVOQ0VTIGB1c2Vyc2AgKGB1c2VyX2lkYCkgT04gREVMRVRFIENBU0NBREUgT04gVVBEQVRFIENBU0NBREVcbikgRU5HSU5FPUlubm9EQiBERUZBVUxUIENIQVJTRVQ9dXRmOG1iNCBDT0xMQVRFPXV0ZjhtYjRfdW5pY29kZV9jaTtcblxuLS0gU0VQQVJBVE9SIC0tXG5cbkNSRUFURSBUQUJMRSBJRiBOT1QgRVhJU1RTIGBjb2Rlc2AgKFxuICBgY29kZV9pZGAgaW50KDExKSBOT1QgTlVMTCBBVVRPX0lOQ1JFTUVOVCxcbiAgYHR5cGVgIHZhcmNoYXIoMTYpIENPTExBVEUgdXRmOG1iNF91bmljb2RlX2NpIERFRkFVTFQgTlVMTCxcbiAgYGRheXNgIGludCgxMSkgREVGQVVMVCBOVUxMIENPTU1FTlQgJ29ubHkgYXBwbGljYWJsZSBpZiB0eXBlIGlzIHJlZGVlbWFibGUnLFxuICBgcGFja2FnZV9pZGAgaW50KDE2KSBERUZBVUxUIE5VTEwsXG4gIGBjb2RlYCB2YXJjaGFyKDMyKSBDT0xMQVRFIHV0ZjhtYjRfdW5pY29kZV9jaSBOT1QgTlVMTCBERUZBVUxUICcnLFxuICBgZGlzY291bnRgIGludCgxMSkgTk9UIE5VTEwsXG4gIGBxdWFudGl0eWAgaW50KDExKSBOT1QgTlVMTCBERUZBVUxUICcxJyxcbiAgYHJlZGVlbWVkYCBpbnQoMTEpIE5PVCBOVUxMIERFRkFVTFQgJzAnLFxuICBgZGF0ZWAgZGF0ZXRpbWUgTk9UIE5VTEwsXG4gIFBSSU1BUlkgS0VZIChgY29kZV9pZGApLFxuICBLRVkgYHR5cGVgIChgdHlwZWApLFxuICBLRVkgYGNvZGVgIChgY29kZWApLFxuICBLRVkgYHBhY2thZ2VfaWRgIChgcGFja2FnZV9pZGApLFxuICBDT05TVFJBSU5UIGBjb2Rlc19pYmZrXzFgIEZPUkVJR04gS0VZIChgcGFja2FnZV9pZGApIFJFRkVSRU5DRVMgYHBhY2thZ2VzYCAoYHBhY2thZ2VfaWRgKSBPTiBERUxFVEUgQ0FTQ0FERSBPTiBVUERBVEUgQ0FTQ0FERVxuKSBFTkdJTkU9SW5ub0RCIERFRkFVTFQgQ0hBUlNFVD11dGY4bWI0IENPTExBVEU9dXRmOG1iNF91bmljb2RlX2NpO1xuXG4tLSBTRVBBUkFUT1IgLS1cblxuQ1JFQVRFIFRBQkxFIElGIE5PVCBFWElTVFMgYHJlZGVlbWVkX2NvZGVzYCAoXG4gIGBpZGAgaW50KDExKSBOT1QgTlVMTCBBVVRPX0lOQ1JFTUVOVCxcbiAgYGNvZGVfaWRgIGludCgxMSkgTk9UIE5VTEwsXG4gIGB1c2VyX2lkYCBpbnQoMTEpIE5PVCBOVUxMLFxuICBgZGF0ZWAgZGF0ZXRpbWUgTk9UIE5VTEwsXG4gIFBSSU1BUlkgS0VZIChgaWRgKSxcbiAgS0VZIGBjb2RlX2lkYCAoYGNvZGVfaWRgKSxcbiAgS0VZIGB1c2VyX2lkYCAoYHVzZXJfaWRgKSxcbiAgQ09OU1RSQUlOVCBgcmVkZWVtZWRfY29kZXNfaWJma18xYCBGT1JFSUdOIEtFWSAoYGNvZGVfaWRgKSBSRUZFUkVOQ0VTIGBjb2Rlc2AgKGBjb2RlX2lkYCkgT04gREVMRVRFIENBU0NBREUgT04gVVBEQVRFIENBU0NBREUsXG4gIENPTlNUUkFJTlQgYHJlZGVlbWVkX2NvZGVzX2liZmtfMmAgRk9SRUlHTiBLRVkgKGB1c2VyX2lkYCkgUkVGRVJFTkNFUyBgdXNlcnNgIChgdXNlcl9pZGApIE9OIERFTEVURSBDQVNDQURFIE9OIFVQREFURSBDQVNDQURFXG4pIEVOR0lORT1Jbm5vREIgREVGQVVMVCBDSEFSU0VUPXV0ZjhtYjQgQ09MTEFURT11dGY4bWI0X3VuaWNvZGVfY2k7XG4ifQ==');

$response = json_decode ($response);

if($response->status == 'error') {
    die(json_encode([
        'status' => 'error',
        'message' => $response->message
    ]));
}

/* Success check */
if($response->status == 'success') {

    /* Prepare the config file content */
    $config_content =
        <<<ALTUM
<?php

/* Configuration of the site */
define('DATABASE_SERVER',   '{$_POST['database_host']}');
define('DATABASE_USERNAME', '{$_POST['database_username']}');
define('DATABASE_PASSWORD', '{$_POST['database_password']}');
define('DATABASE_NAME',     '{$_POST['database_name']}');
define('SITE_URL',          '{$_POST['url']}');

ALTUM;

    /* Write the new config file */
    file_put_contents(ROOT . 'app/config/config.php', $config_content);

    /* Run SQL */
    $dump_content = file_get_contents(ROOT . 'install/dump.sql');

    $dump = explode('-- SEPARATOR --', $dump_content);

    foreach($dump as $query) {
        $database->query($query);

        if($database->error) {
            die(json_encode([
                'status' => 'error',
                'message' => 'Error when running the database queries: ' . $database->error
            ]));
        }
    }

    /* Run external SQL if needed */
    if(!empty($response->sql)) {
        $dump = explode('-- SEPARATOR --', $response->sql);

        foreach($dump as $query) {
            $database->query($query);

            if($database->error) {
                die(json_encode([
                    'status' => 'error',
                    'message' => 'Error when running the database queries: ' . $database->error
                ]));
            }
        }
    }

    /* Create the installed file */
    file_put_contents(ROOT . 'install/installed', '');

    die(json_encode([
        'status' => 'success',
        'message' => ''
    ]));
}
