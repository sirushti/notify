<?php defined('ALTUMCODE') || die() ?>

<div class="d-flex justify-content-between mb-4">
    <div class="d-flex align-items-center">
        <h1 class="h3 mr-3"><i class="fa fa-fw fa-xs fa-box-open text-primary-900 mr-2"></i> <?= sprintf($this->language->admin_package_update->header, $data->package->name) ?></h1>

        <?= get_admin_options_button('package', $data->package->package_id) ?>
    </div>
</div>

<?php display_notifications() ?>

<div class="card">
    <div class="card-body">

        <form action="" method="post" role="form">
            <input type="hidden" name="token" value="<?= \Altum\Middlewares\Csrf::get() ?>" />
            <input type="hidden" name="type" value="update" />

            <div class="row">
                <div class="col-12 col-md-4">
                    <h2 class="h4"><?= $this->language->admin_packages->main->header ?></h2>
                    <p class="text-muted"><?= $this->language->admin_packages->main->subheader ?></p>
                </div>

                <div class="col">
                    <div class="form-group">
                        <label for="name"><?= $this->language->admin_packages->main->name ?></label>
                        <input type="text" id="name" name="name" class="form-control form-control-lg" value="<?= $data->package->name ?>" />
                    </div>

                    <div class="form-group">
                        <label for="status"><?= $this->language->admin_packages->main->status ?></label>
                        <select id="status" name="status" class="form-control form-control-lg">
                            <option value="1" <?= $data->package->status == 1 ? 'selected="true"' : null ?>><?= $this->language->global->active ?></option>
                            <option value="0" <?= $data->package->status == 0 ? 'selected="true"' : null ?>><?= $this->language->global->disabled ?></option>
                            <option value="2" <?= $data->package->status == 2 ? 'selected="true"' : null ?>><?= $this->language->global->hidden ?></option>
                        </select>
                    </div>

                    <?php if($data->package_id == 'trial'): ?>
                        <div class="form-group">
                            <div class="form-group">
                                <label for="days"><?= $this->language->admin_packages->main->trial->days ?></label>
                                <input type="text" id="days" name="days" class="form-control form-control-lg" value="<?= $data->package->days ?>" />
                                <div><small class="text-muted"><?= $this->language->admin_packages->main->trial->days_help ?></small></div>
                            </div>
                        </div>
                    <?php endif ?>

                    <?php if(is_numeric($data->package_id)): ?>
                        <div class="row">
                            <div class="col-sm-12 col-md-6">
                                <div class="form-group">
                                    <div class="form-group">
                                        <label for="monthly_price"><?= sprintf($this->language->admin_packages->main->monthly_price, $this->settings->payment->currency) ?></label>
                                        <input type="text" id="monthly_price" name="monthly_price" class="form-control form-control-lg" value="<?= $data->package->monthly_price ?>" />
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-12 col-md-6">
                                <div class="form-group">
                                    <label for="annual_price"><?= sprintf($this->language->admin_packages->main->annual_price, $this->settings->payment->currency) ?></label>
                                    <input type="text" id="annual_price" name="annual_price" class="form-control form-control-lg" value="<?= $data->package->annual_price ?>" />
                                </div>
                            </div>
                        </div>
                    <?php endif ?>

                </div>
            </div>

            <div class="mt-5"></div>

            <div class="row">
                <div class="col-12 col-md-4">
                    <h2 class="h4"><?= $this->language->admin_packages->package->header ?></h2>
                    <p class="text-muted"><?= $this->language->admin_packages->package->subheader ?></p>
                </div>

                <div class="col">
                    <div class="form-group">
                        <label for="campaigns_limit"><?= $this->language->admin_packages->package->campaigns_limit ?></label>
                        <input type="number" id="campaigns_limit" name="campaigns_limit" min="-1" class="form-control form-control-lg" value="<?= $data->package->settings->campaigns_limit ?>" />
                        <small class="text-muted"><?= $this->language->admin_packages->package->campaigns_limit_help ?></small>
                    </div>

                    <div class="form-group">
                        <label for="notifications_limit"><?= $this->language->admin_packages->package->notifications_limit ?></label>
                        <input type="number" id="notifications_limit" name="notifications_limit" min="-1" class="form-control form-control-lg" value="<?= $data->package->settings->notifications_limit ?>" />
                        <small class="text-muted"><?= $this->language->admin_packages->package->notifications_limit_help ?></small>
                    </div>

                    <div class="form-group">
                        <label for="notifications_impressions_limit"><?= $this->language->admin_packages->package->notifications_impressions_limit ?> <small class="text-muted"><?= $this->language->admin_packages->package->per_month ?></small></label>
                        <input type="number" id="notifications_impressions_limit" name="notifications_impressions_limit" min="-1" class="form-control form-control-lg" value="<?= $data->package->settings->notifications_impressions_limit ?>" />
                        <small class="text-muted"><?= $this->language->admin_packages->package->notifications_impressions_limit_help ?></small>
                    </div>

                    <div class="custom-control custom-switch mb-3">
                        <input id="no_ads" name="no_ads" type="checkbox" class="custom-control-input" <?= $data->package->settings->no_ads ? 'checked="true"' : null ?>>
                        <label class="custom-control-label" for="no_ads"><?= $this->language->admin_packages->package->no_ads ?></label>
                        <div><small class="text-muted"><?= $this->language->admin_packages->package->no_ads_help ?></small></div>
                    </div>

                    <div class="custom-control custom-switch mb-3">
                        <input id="removable_branding" name="removable_branding" type="checkbox" class="custom-control-input" <?= $data->package->settings->removable_branding ? 'checked="true"' : null ?>>
                        <label class="custom-control-label" for="removable_branding"><?= $this->language->admin_packages->package->removable_branding ?></label>
                        <div><small class="text-muted"><?= $this->language->admin_packages->package->removable_branding_help ?></small></div>
                    </div>

                    <div class="custom-control custom-switch mb-3">
                        <input id="custom_branding" name="custom_branding" type="checkbox" class="custom-control-input" <?= $data->package->settings->custom_branding ? 'checked="true"' : null ?>>
                        <label class="custom-control-label" for="custom_branding"><?= $this->language->admin_packages->package->custom_branding ?></label>
                        <div><small class="text-muted"><?= $this->language->admin_packages->package->custom_branding_help ?></small></div>
                    </div>

                    <h3 class="h5 mt-4"><?= $this->language->admin_packages->package->enabled_notifications ?></h3>
                    <p class="text-muted"><?= $this->language->admin_packages->package->enabled_notifications_help ?></p>

                    <div class="row">
                        <?php foreach($data->notifications as $notification_type => $notification_config): ?>
                            <div class="col-6 mb-3">
                                <div class="custom-control custom-switch">
                                    <input id="enabled_notifications_<?= $notification_type ?>" name="enabled_notifications[]" value="<?= $notification_type ?>" type="checkbox" class="custom-control-input" <?= $data->package->settings->enabled_notifications->{$notification_type} ? 'checked="true"' : null ?>>
                                    <label class="custom-control-label" for="enabled_notifications_<?= $notification_type ?>"><?= $this->language->notification->{strtolower($notification_type)}->name ?></label>
                                </div>
                            </div>
                        <?php endforeach ?>
                    </div>
                </div>
            </div>

            <div class="row mt-4">
                <div class="col-12 col-md-4"></div>

                <div class="col">
                    <button type="submit" name="submit" class="btn btn-primary"><?= $this->language->global->update ?></button>
                </div>
            </div>
        </form>

    </div>
</div>

<div class="card mt-5">
    <div class="card-body">
        <div class="d-flex justify-content-between align-items-center">
            <div>
                <h1 class="h3"><?= $this->language->admin_package_update->update_users_package_settings->header ?></h1>
                <p class="text-muted"><?= $this->language->admin_package_update->update_users_package_settings->subheader ?></p>
            </div>

            <form action="" method="post" role="form">
                <input type="hidden" name="token" value="<?= \Altum\Middlewares\Csrf::get() ?>" />
                <input type="hidden" name="type" value="update_users_package_settings" />

                <button type="submit" name="submit" class="btn btn-outline-secondary"><?= $this->language->global->update ?></button>
            </form>
        </div>
    </div>
</div>

