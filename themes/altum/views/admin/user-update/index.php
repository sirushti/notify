<?php defined('ALTUMCODE') || die() ?>

<div class="d-flex justify-content-between mb-4">
    <div class="d-flex align-items-center">
        <h1 class="h3 mr-3"><i class="fa fa-fw fa-xs fa-user text-primary-900 mr-2"></i> <?= $this->language->admin_user_update->header ?></h1>

        <?= get_admin_options_button('user', $data->user->user_id) ?>
    </div>
</div>

<?php display_notifications() ?>

<div class="card">
    <div class="card-body">

        <form action="" method="post" role="form" enctype="multipart/form-data">
            <input type="hidden" name="token" value="<?= \Altum\Middlewares\Csrf::get() ?>" />

            <div class="row">
                <div class="col-12 col-md-4">
                    <h2 class="h4"><?= $this->language->admin_user_update->main->header ?></h2>
                    <p class="text-muted"><?= $this->language->admin_user_update->main->subheader ?></p>
                </div>

                <div class="col">
                    <div class="form-group">
                        <label><?= $this->language->admin_user_update->main->name ?></label>
                        <input type="text" name="name" class="form-control form-control-lg" value="<?= $data->user->name ?>" />
                    </div>

                    <div class="form-group">
                        <label><?= $this->language->admin_user_update->main->email ?></label>
                        <input type="text" name="email" class="form-control form-control-lg" value="<?= $data->user->email ?>" />
                    </div>

                    <div class="form-group">
                        <label><?= $this->language->admin_user_update->main->status ?></label>

                        <select class="form-control form-control-lg" name="status">
                            <option value="1" <?php if($data->user->active == 1) echo 'selected' ?>><?= $this->language->admin_user_update->main->status_active ?></option>
                            <option value="0" <?php if($data->user->active == 0) echo 'selected' ?>><?= $this->language->admin_user_update->main->status_disabled ?></option>
                        </select>
                    </div>

                    <div class="form-group">
                        <label><?= $this->language->admin_user_update->main->type ?></label>

                        <select class="form-control form-control-lg" name="type">
                            <option value="1" <?php if($data->user->type == 1) echo 'selected' ?>><?= $this->language->admin_user_update->main->type_admin ?></option>
                            <option value="0" <?php if($data->user->type == 0) echo 'selected' ?>><?= $this->language->admin_user_update->main->type_user ?></option>
                        </select>

                        <small class="text-muted"><?= $this->language->admin_user_update->main->type_help ?></small>
                    </div>
                </div>
            </div>

                <div class="mt-5"></div>

                <div class="row">
                    <div class="col-12 col-md-4">
                        <h2 class="h4"><?= $this->language->admin_user_update->package->header ?></h2>
                        <p class="text-muted"><?= $this->language->admin_user_update->package->header_help ?></p>
                    </div>

                    <div class="col">
                        <div class="form-group">
                            <label><?= $this->language->admin_user_update->package->package_id ?></label>

                            <select class="form-control form-control-lg" name="package_id">
                                <option value="free" <?php if($data->user->package->package_id == 'free') echo 'selected' ?>><?= $this->settings->package_free->name ?></option>
                                <option value="trial" <?php if($data->user->package->package_id == 'trial') echo 'selected' ?>><?= $this->settings->package_trial->name ?></option>
                                <option value="custom" <?php if($data->user->package->package_id == 'custom') echo 'selected' ?>><?= $this->settings->package_custom->name ?></option>

                                <?php while($row = $data->packages_result->fetch_object()): ?>
                                    <option value="<?= $row->package_id ?>" <?php if($data->user->package->package_id == $row->package_id) echo 'selected' ?>><?= $row->name ?></option>
                                <?php endwhile ?>
                            </select>
                        </div>

                        <div class="form-group">
                            <label><?= $this->language->admin_user_update->package->package_trial_done ?></label>

                            <select class="form-control form-control-lg" name="package_trial_done">
                                <option value="1" <?= $data->user->package_trial_done ? 'selected="selected"' : null ?>><?= $this->language->global->yes ?></option>
                                <option value="0" <?= !$data->user->package_trial_done ? 'selected="selected"' : null ?>><?= $this->language->global->no ?></option>
                            </select>
                        </div>

                        <div id="package_expiration_date_container" class="form-group">
                            <label><?= $this->language->admin_user_update->package->package_expiration_date ?></label>
                            <input type="text" class="form-control form-control-lg" name="package_expiration_date" autocomplete="off" value="<?= $data->user->package_expiration_date ?>">
                        </div>

                        <div id="package_settings" style="display: none">
                            <div class="form-group">
                                <label for="campaigns_limit"><?= $this->language->admin_packages->package->campaigns_limit ?></label>
                                <input type="number" id="campaigns_limit" name="campaigns_limit" min="-1" class="form-control form-control-lg" value="<?= $data->user->package->settings->campaigns_limit ?>" />
                                <small class="text-muted"><?= $this->language->admin_packages->package->campaigns_limit_help ?></small>
                            </div>

                            <div class="form-group">
                                <label for="notifications_limit"><?= $this->language->admin_packages->package->notifications_limit ?></label>
                                <input type="number" id="notifications_limit" name="notifications_limit" min="-1" class="form-control form-control-lg" value="<?= $data->user->package->settings->notifications_limit ?>" />
                                <small class="text-muted"><?= $this->language->admin_packages->package->notifications_limit_help ?></small>
                            </div>

                            <div class="form-group">
                                <label for="notifications_impressions_limit"><?= $this->language->admin_packages->package->notifications_impressions_limit ?></label>
                                <input type="number" id="notifications_impressions_limit" name="notifications_impressions_limit" min="-1" class="form-control form-control-lg" value="<?= $data->user->package->settings->notifications_impressions_limit ?>" />
                                <small class="text-muted"><?= $this->language->admin_packages->package->notifications_impressions_limit_help ?></small>
                            </div>

                            <div class="custom-control custom-switch mb-3">
                                <input id="no_ads" name="no_ads" type="checkbox" class="custom-control-input" <?= $data->user->package->settings->no_ads ? 'checked="true"' : null ?>>
                                <label class="custom-control-label" for="no_ads"><?= $this->language->admin_packages->package->no_ads ?></label>
                                <div><small class="text-muted"><?= $this->language->admin_packages->package->no_ads_help ?></small></div>
                            </div>

                            <div class="custom-control custom-switch mb-3">
                                <input id="removable_branding" name="removable_branding" type="checkbox" class="custom-control-input" <?= $data->user->package->settings->removable_branding ? 'checked="true"' : null ?>>
                                <label class="custom-control-label" for="removable_branding"><?= $this->language->admin_packages->package->removable_branding ?></label>
                                <div><small class="text-muted"><?= $this->language->admin_packages->package->removable_branding_help ?></small></div>
                            </div>

                            <div class="custom-control custom-switch mb-3">
                                <input id="custom_branding" name="custom_branding" type="checkbox" class="custom-control-input" <?= $data->user->package->settings->custom_branding ? 'checked="true"' : null ?>>
                                <label class="custom-control-label" for="custom_branding"><?= $this->language->admin_packages->package->custom_branding ?></label>
                                <div><small class="text-muted"><?= $this->language->admin_packages->package->custom_branding_help ?></small></div>
                            </div>

                            <h3 class="h5 mt-3"><?= $this->language->admin_packages->package->enabled_notifications ?></h3>
                            <p class="text-muted"><?= $this->language->admin_packages->package->enabled_notifications_help ?></p>

                            <div class="row">
                                <?php foreach($data->notifications as $notification_type => $notification_config): ?>
                                    <div class="col-6">
                                        <div class="custom-control custom-switch">
                                            <input id="enabled_notifications_<?= $notification_type ?>" name="enabled_notifications[]" value="<?= $notification_type ?>" type="checkbox" class="custom-control-input" <?= $data->user->package->settings->enabled_notifications->{$notification_type} ? 'checked="true"' : null ?>>
                                            <label class="custom-control-label" for="enabled_notifications_<?= $notification_type ?>"><?= $this->language->notification->{strtolower($notification_type)}->name ?></label>
                                        </div>
                                    </div>
                                <?php endforeach ?>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="mt-5"></div>

                <div class="row">
                    <div class="col-12 col-md-4">
                        <h2 class="h4"><?= $this->language->admin_user_update->change_password->header ?></h2>
                        <p class="text-muted"><?= $this->language->admin_user_update->change_password->subheader ?></p>
                    </div>

                    <div class="col">
                        <div class="form-group">
                            <label><?= $this->language->admin_user_update->change_password->new_password ?></label>
                            <input type="password" name="new_password" class="form-control form-control-lg" />
                        </div>

                        <div class="form-group">
                            <label><?= $this->language->admin_user_update->change_password->repeat_password ?></label>
                            <input type="password" name="repeat_password" class="form-control form-control-lg" />
                        </div>
                    </div>
                </div>

                <div class="row mt-4">
                    <div class="col-12 col-md-4"></div>

                    <div class="col">
                        <button type="submit" name="submit" class="btn btn-primary"><?= $this->language->global->update ?></button>
                    </div>
                </div>

        </form>
    </div>
</div>

<?php ob_start() ?>
<link href="<?= SITE_URL . ASSETS_URL_PATH . 'css/datepicker.min.css' ?>" rel="stylesheet" media="screen">
<?php \Altum\Event::add_content(ob_get_clean(), 'head') ?>

<?php ob_start() ?>
<script src="<?= SITE_URL . ASSETS_URL_PATH . 'js/libraries/datepicker.min.js' ?>"></script>
<script>
    let check_package_id = () => {
        let selected_package_id = $('[name="package_id"]').find(':selected').attr('value');

        if(selected_package_id == 'free') {
            $('#package_expiration_date_container').hide();
        } else {
            $('#package_expiration_date_container').show();
        }

        if(selected_package_id == 'custom') {
            $('#package_settings').show();
        } else {
            $('#package_settings').hide();
        }
    };

    /* Initial check */
    check_package_id();

    $.fn.datepicker.language['altum'] = <?= json_encode(require APP_PATH . 'includes/datepicker_translations.php') ?>;
    $('[name="package_expiration_date"]').datepicker({
        classes: 'datepicker-modal',
        language: 'altum',
        dateFormat: 'yyyy-mm-dd',
        autoClose: true,
        timepicker: false,
        toggleSelected: false,
        minDate: new Date()
    });

    /* Dont show expiration date when the chosen package is the free one */
    $('[name="package_id"]').on('change', check_package_id);
</script>
<?php \Altum\Event::add_content(ob_get_clean(), 'javascript') ?>
