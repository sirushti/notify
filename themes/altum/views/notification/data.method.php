<?php defined('ALTUMCODE') || die(); ?>

<div class="margin-top-3 mb-3">
    <div class="d-flex flex-column flex-md-row justify-content-between">
        <div class="d-flex">
            <h2 class="h3 mr-3"><?= $this->language->notification->data->header ?></h2>

            <div>
                <?php if(count($data->conversions)): ?>
                    <a href="<?= url('notification/' . $data->notification->notification_id . '/data/' . $data->date->start_date . '/' . $data->date->end_date . '?json') ?>" target="_blank" class="btn btn-sm btn-light rounded-pill mr-3"><i class="fa fa-fw fa-file-csv"></i> <?= $this->language->global->export_json ?></a>
                <?php endif ?>
                <button type="button" data-toggle="modal" data-target="#create_notification_data" class="btn btn-sm btn-primary rounded-pill mr-3"><i class="fa fa-plus-circle"></i> <?= $this->language->notification->data->create ?></button>
            </div>
        </div>

        <div>
            <form class="form-inline" id="datepicker_form">

                <label class="position-relative">
                    <div id="datepicker_selector" class="text-muted clickable">
                            <span class="mr-1">
                                <?php if($data->date->start_date == $data->date->end_date): ?>
                                    <?= \Altum\Date::get($data->date->start_date, 2, \Altum\Date::$default_timezone) ?>
                                <?php else: ?>
                                    <?= \Altum\Date::get($data->date->start_date, 2, \Altum\Date::$default_timezone) . ' - ' . \Altum\Date::get($data->date->end_date, 2, \Altum\Date::$default_timezone) ?>
                                <?php endif ?>
                            </span>
                        <i class="fa fa-fw fa-caret-down"></i>
                    </div>

                    <input
                            type="text"
                            id="datepicker_input"
                            data-range="true"
                            data-min="<?= (new \DateTime($data->notification->date))->format('Y-m-d') ?>"
                            name="date_range"
                            value="<?= $data->date->input_date_range ? $data->date->input_date_range : '' ?>"
                            placeholder=""
                            autocomplete="off"
                            readonly="readonly"
                            class="custom-control-input"
                    >

                </label>
            </form>
        </div>
    </div>
</div>


<?php if(!count($data->conversions)): ?>

    <div class="d-flex flex-column align-items-center justify-content-center">
        <img src="<?= SITE_URL . ASSETS_URL_PATH . 'images/no_data.svg' ?>" class="col-10 col-md-6 col-lg-4 mb-3" alt="<?= $this->language->global->no_data ?>" />
        <h2 class="h4 text-muted"><?= $this->language->global->no_data ?></h2>
        <p><?= $this->language->notification->info_message->no_data ?></a></p>
    </div>

<?php else: ?>

    <div class="table-responsive table-custom-container">
        <table class="table table-custom">
            <thead>
            <tr>
                <th><?= $this->language->notification->data->data ?></th>
                <th><?= $this->language->notification->data->type ?></th>
                <th><?= $this->language->notification->data->date ?></th>
                <th></th>
            </tr>
            </thead>
            <tbody class="accordion" id="accordion">

            <?php foreach($data->conversions as $row): ?>
                <tr class="clickable" data-toggle="collapse" data-target="#<?= 'data_collapse_' . $row->id ?>" aria-expanded="true" aria-controls="<?= 'data_collapse_' . $row->id ?>">
                    <td>
                        <strong><?= $this->language->notification->data->expand_data ?></strong>
                    </td>
                    <td><?= $this->language->notification->data->{'type_' . $row->type} ?></td>
                    <td><span class="text-muted" data-toggle="tooltip" title="<?= \Altum\Date::get($row->date, 1) ?>"><?= \Altum\Date::get($row->date) ?></span></td>
                    <td>
                        <div class="dropdown">
                            <a href="#" data-toggle="dropdown" class="text-secondary dropdown-toggle dropdown-toggle-simple">
                                <i class="fa fa-ellipsis-v"></i>

                                <div class="dropdown-menu dropdown-menu-right">
                                    <a href="#" class="dropdown-item" data-delete-notification-data="<?= $this->language->global->info_message->confirm_delete ?>" data-row-id="<?= $row->id ?>"><i class="fa fa-fw fa-times"></i> <?= $this->language->global->delete ?></a>
                                </div>
                            </a>
                        </div>
                    </td>
                </tr>

                <tr id="<?= 'data_collapse_' . $row->id ?>" data-id="<?= $row->id ?>" data-notification-id="<?= $row->notification_id ?>" class="collapse" data-parent="#accordion">
                    <td colspan="4">
                        <div class="row">
                            <div class="d-flex justify-content-center">
                                <div class="spinner-grow"><span class="sr-only">Loading...</span></div>
                            </div>
                        </div>
                    </td>
                </tr>

            <?php endforeach ?>

            </tbody>
        </table>
    </div>

<?php endif ?>


<?php ob_start() ?>
<script src="<?= SITE_URL . ASSETS_URL_PATH . 'js/libraries/datepicker.min.js' ?>"></script>

<script>
    /* Datepicker */
    $.fn.datepicker.language['altum'] = <?= json_encode(require APP_PATH . 'includes/datepicker_translations.php') ?>;
    let datepicker = $('#datepicker_input').datepicker({
        language: 'altum',
        dateFormat: 'yyyy-mm-dd',
        autoClose: true,
        timepicker: false,
        toggleSelected: false,
        minDate: new Date($('#datepicker_input').data('min')),
        maxDate: new Date(),

        onSelect: (formatted_date, date) => {

            if(date.length > 1) {
                let [ start_date, end_date ] = formatted_date.split(',');

                if(typeof end_date == 'undefined') {
                    end_date = start_date
                }

                /* Redirect */
                redirect(`${$('#base_controller_url').val()}/data/${start_date}/${end_date}`, true);
            }
        }
    });

    /* Handle the opening and closing of the details for conversions */
    $('[id^="data_collapse_"]').on('show.bs.collapse', event => {
        let id = $(event.currentTarget).data('id');
        let notification_id = $(event.currentTarget).data('notification-id');
        let global_token = $('input[name="global_token"]').val();
        let request_type = 'read_data_conversion';

        $.ajax({
            type: 'GET',
            url: `notifications-ajax?id=${id}&notification_id=${notification_id}&global_token=${global_token}&request_type=${request_type}`,
            success: (result) => {

                $(event.currentTarget).find('.row').html(result.details.html);

                /* Refresh tooltips */
                $('[data-toggle="tooltip"]').tooltip();

            },
            dataType: 'json'
        });


    });

    /* Delete handler for the conversion */
    $('[data-delete-notification-data]').on('click', event => {
        let message = $(event.currentTarget).attr('data-delete-notification-data');

        if(!confirm(message)) return false;

        /* Continue with the deletion */
        ajax_call_helper(event, 'notification-data-ajax', 'delete', () => {

            /* On success delete the actual row from the DOM */
            let current_tr = $(event.currentTarget).closest('tr');
            let next_tr = current_tr.next();

            current_tr.remove();
            next_tr.remove();

        });
    });
</script>

<?php \Altum\Event::add_content(ob_get_clean(), 'javascript') ?>
