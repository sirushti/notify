<?php
defined('ALTUMCODE') || die();

/* Include the extra content of the notification */
$statistics = require THEME_PATH . 'views/notification/statistics/statistics.' . strtolower($data->notification->type) . '.method.php';

?>

<?php if(!$this->settings->socialproofo->analytics_is_enabled): ?>
    <div class="alert alert-warning" role="alert">
        <?= $this->language->notification->statistics->disabled ?>
    </div>
<?php endif ?>

<div class="margin-top-3 mb-3">
    <div class="d-flex flex-column flex-md-row justify-content-between">
        <div>
            <h2 class="h3"><?= $this->language->notification->statistics->header ?></h2>
        </div>

        <div>
            <form class="form-inline" id="datepicker_form">
                <label class="position-relative">
                    <div id="datepicker_selector" class="text-muted clickable">
                            <span class="mr-1">
                                <?php if($data->date->start_date == $data->date->end_date): ?>
                                    <?= \Altum\Date::get($data->date->start_date, 2, \Altum\Date::$default_timezone) ?>
                                <?php else: ?>
                                    <?= \Altum\Date::get($data->date->start_date, 2, \Altum\Date::$default_timezone) . ' - ' . \Altum\Date::get($data->date->end_date, 2, \Altum\Date::$default_timezone) ?>
                                <?php endif ?>
                            </span>
                        <i class="fa fa-fw fa-caret-down"></i>
                    </div>

                    <input
                            type="text"
                            id="datepicker_input"
                            data-range="true"
                            data-min="<?= (new \DateTime($data->notification->date))->format('Y-m-d') ?>"
                            name="date_range"
                            value="<?= $data->date->input_date_range ? $data->date->input_date_range : '' ?>"
                            placeholder=""
                            autocomplete="off"
                            readonly="readonly"
                            class="custom-control-input"
                    >

                </label>
            </form>
        </div>
    </div>
</div>

<?php if(!count($data->logs)): ?>

    <div class="d-flex flex-column align-items-center justify-content-center">
        <img src="<?= SITE_URL . ASSETS_URL_PATH . 'images/no_data.svg' ?>" class="col-10 col-md-6 col-lg-4 mb-3" alt="<?= $this->language->global->no_data ?>" />
        <h2 class="h4 text-muted"><?= $this->language->global->no_data ?></h2>
        <p><?= $this->language->notification->info_message->no_data ?></a></p>
    </div>

<?php else: ?>

    <div class="row justify-content-between">
        <div class="col-12 col-md-4 mb-3 mb-xl-0">
            <div class="card border-0 h-100">
                <div class="card-body d-flex">

                    <div>
                        <div class="card border-0 bg-primary-200 text-primary-700 mr-3">
                            <div class="p-3 d-flex align-items-center justify-content-between">
                                <i class="fa fa-fw fa-eye fa-lg"></i>
                            </div>
                        </div>
                    </div>

                    <div>
                        <div class="card-title h4 m-0"><?= nr($data->logs_total['impression']) ?></div>
                        <small class="text-muted"><?= $this->language->notification->statistics->impressions_chart ?></small>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-12 col-md-4 mb-3 mb-xl-0">
            <div class="card border-0 h-100">
                <div class="card-body d-flex">

                    <div>
                        <div class="card border-0 bg-primary-200 text-primary-700 mr-3">
                            <div class="p-3 d-flex align-items-center justify-content-between">
                                <i class="fa fa-fw fa-mouse-pointer fa-lg"></i>
                            </div>
                        </div>
                    </div>

                    <div>
                        <div class="card-title h4 m-0"><?= nr($data->logs_total['hover']) ?></div>
                        <small class="text-muted"><?= $this->language->notification->statistics->hovers_chart ?></small>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-12 col-md-4 mb-3 mb-xl-0">
            <div class="card border-0 h-100">
                <div class="card-body d-flex">

                    <div>
                        <div class="card border-0 bg-primary-200 text-primary-700 mr-3">
                            <div class="p-3 d-flex align-items-center justify-content-between">
                                <i class="fa fa-fw fa-mouse fa-lg"></i>
                            </div>
                        </div>
                    </div>

                    <div>
                        <div class="card-title h4 m-0"><?= nr($data->logs_total['click']) ?></div>
                        <small class="text-muted"><?= $this->language->notification->statistics->clicks_chart ?></small>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="chart-container">
        <canvas id="impressions_chart"></canvas>
    </div>

    <div class="chart-container">
        <canvas id="hovers_chart"></canvas>
    </div>

    <?php foreach($statistics->html as $html): echo $html; endforeach ?>

    <?php if($data->top_pages_result->num_rows): ?>

        <h2 class="h3 margin-top-3"><?= $this->language->notification->statistics->header_top_pages ?></h2>
        <p class="text-muted"><?= $this->language->notification->statistics->subheader_top_pages ?></p>

        <div class="table-responsive table-custom-container">
            <table class="table table-custom">
                <thead>
                <tr>
                    <th></th>
                    <th>
                        <?= $this->language->notification->statistics->total_uniques ?>
                        <span data-toggle="tooltip" title="<?= $this->language->notification->statistics->total_uniques_help ?>"><i class="fa fa-fw fa-question-circle text-muted"></i></span>
                    </th>
                    <th>
                        <?= $this->language->notification->statistics->total_sessions ?>
                        <span data-toggle="tooltip" title="<?= $this->language->notification->statistics->total_sessions_help ?>"><i class="fa fa-fw fa-question-circle text-muted"></i></span>
                    </th>
                </tr>
                </thead>
                <tbody>

                <?php while($row = $data->top_pages_result->fetch_object()): ?>

                    <tr>
                        <td>
                            <div class="d-flex flex-column">
                                <?= $this->language->notification->statistics->{$row->type} ?>
                                <span class="text-muted"><?= $row->url ?></span>
                            </div>
                        </td>
                        <td><?= nr($row->total_uniques) ?></td>
                        <td><?= nr($row->total_sessions) ?></td>
                    </tr>

                <?php endwhile ?>

                </tbody>
            </table>
        </div>

    <?php endif ?>

<?php endif ?>

<?php ob_start() ?>
<script src="<?= SITE_URL . ASSETS_URL_PATH . 'js/libraries/datepicker.min.js' ?>"></script>
<script src="<?= SITE_URL . ASSETS_URL_PATH . 'js/libraries/Chart.bundle.min.js' ?>"></script>

<script>
    /* Datepicker */
    $.fn.datepicker.language['altum'] = <?= json_encode(require APP_PATH . 'includes/datepicker_translations.php') ?>;
    let datepicker = $('#datepicker_input').datepicker({
        language: 'altum',
        dateFormat: 'yyyy-mm-dd',
        autoClose: true,
        timepicker: false,
        toggleSelected: false,
        minDate: new Date($('#datepicker_input').data('min')),
        maxDate: new Date(),

        onSelect: (formatted_date, date) => {

            if(date.length > 1) {
                let [ start_date, end_date ] = formatted_date.split(',');

                if(typeof end_date == 'undefined') {
                    end_date = start_date
                }

                /* Redirect */
                redirect(`${$('#base_controller_url').val()}/statistics/${start_date}/${end_date}`, true);
            }
        }
    });

    <?php if(count($data->logs)): ?>
    /* Charts */
    Chart.defaults.global.elements.line.borderWidth = 4;
    Chart.defaults.global.elements.point.radius = 3;
    Chart.defaults.global.elements.point.borderWidth = 7;

    let impressions_chart = document.getElementById('impressions_chart').getContext('2d');

    let gradient = impressions_chart.createLinearGradient(0, 0, 0, 250);
    gradient.addColorStop(0, 'rgba(43, 227, 155, 0.6)');
    gradient.addColorStop(1, 'rgba(43, 227, 155, 0.05)');

    new Chart(impressions_chart, {
        type: 'line',
        data: {
            labels: <?= $data->logs_chart['labels'] ?>,
            datasets: [{
                data: <?= $data->logs_chart['impression'] ?? '[]' ?>,
                backgroundColor: gradient,
                borderColor: '#2BE39B',
                fill: true
            }]
        },
        options: {
            tooltips: {
                mode: 'index',
                intersect: false,
                callbacks: {
                    label: (tooltipItem, data) => {
                        let value = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];

                        return nr(value);
                    }
                }
            },
            title: {
                display: true,
                text: <?= json_encode($this->language->notification->statistics->impressions_chart) ?>
            },
            legend: {
                display: false
            },
            responsive: true,
            maintainAspectRatio: false,
            scales: {
                yAxes: [{
                    gridLines: {
                        display: false
                    },
                    ticks: {
                        userCallback: (value, index, values) => {
                            if (Math.floor(value) === value) {
                                return nr(value);
                            }
                        }
                    },
                    min: 0
                }],
                xAxes: [{
                    gridLines: {
                        display: false
                    }
                }]
            }
        }
    });


    let hovers_chart = document.getElementById('hovers_chart').getContext('2d');

    gradient = hovers_chart.createLinearGradient(0, 0, 0, 250);
    gradient.addColorStop(0, 'rgba(62, 193, 255, 0.6)');
    gradient.addColorStop(1, 'rgba(62, 193, 255, 0.05)');

    new Chart(hovers_chart, {
        type: 'line',
        data: {
            labels: <?= $data->logs_chart['labels'] ?>,
            datasets: [{
                data: <?= $data->logs_chart['hover'] ?? '[]' ?>,
                backgroundColor: gradient,
                borderColor: '#3ec1ff',
                fill: true
            }]
        },
        options: {
            tooltips: {
                mode: 'index',
                intersect: false,
                callbacks: {
                    label: (tooltipItem, data) => {
                        let value = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];

                        return nr(value);
                    }
                }
            },
            title: {
                display: true,
                text: <?= json_encode($this->language->notification->statistics->hovers_chart) ?>
            },
            legend: {
                display: false
            },
            responsive: true,
            maintainAspectRatio: false,
            scales: {
                yAxes: [{
                    gridLines: {
                        display: false
                    },
                    ticks: {
                        userCallback: (value, index, values) => {
                            if (Math.floor(value) === value) {
                                return nr(value);
                            }
                        }
                    },
                    min: 0
                }],
                xAxes: [{
                    gridLines: {
                        display: false
                    }
                }]
            }
        }
    });
    <?php endif ?>

</script>

<?= $statistics->javascript ?>

<?php \Altum\Event::add_content(ob_get_clean(), 'javascript') ?>

